"use client";

import React from "react";
import ButtonPrimary from "@/shared/Button/ButtonPrimary";
import LikeButton from "@/components/LikeButton";
import BagIcon from "@/components/BagIcon";
import detail1JPG from "@/images/products/detail1.jpg";
import detail2JPG from "@/images/products/detail2.jpg";
import detail3JPG from "@/images/products/detail3.jpg";
import Image from "next/image";

const LIST_IMAGES_DEMO = [detail1JPG, detail2JPG, detail3JPG];

const ProductDetailPage = () => {
  const renderSectionContent = () => {
    return (
      <div className="space-y-7 2xl:space-y-8">
        {/* ---------- 1 HEADING ----------  */}
        <p className="text-white">8200437</p>
        <p className="text-white py-2">
          {
            "Gibson Custom Shop Murphy Lab 1957 SJ-200 'Light Aged Vintage Sunburst' Acoustic Guitar ."
          }
        </p>
        <div>
          <h3 className="sm:text-3xl font-semibold text-white">
            {"2.700.000 VND"}
          </h3>
        </div>

        {/*  ---------- 4  QTY AND ADD TO CART BUTTON */}
        <div className="flex space-x-3.5">
          <ButtonPrimary className="flex-1 flex-shrink-0" onClick={() => {}}>
            <BagIcon className="hidden sm:inline-block w-5 h-5 mb-0.5" />
            <span className="ml-3">Buy now</span>
          </ButtonPrimary>
        </div>

        {/*  */}
        {/*  */}

        <p className="text-white">Available at Acoustic Centre Melbourne</p>
        <p className="text-white">
          Pickup available at Acoustic Centre South Melbourne
        </p>
      </div>
    );
  };

  const renderDetailSection = () => {
    return (
      <div className="py-20">
        <h2 className="text-2xl font-semibold text-white">Product Details</h2>
        <div className="prose prose-sm sm:prose dark:prose-invert sm:max-w-4xl mt-7 text-white">
          <p>
            {
              "The mother-of-pearl Graduated Crown inlays on the rosewood fingerboard are a testament to the meticulous craftsmanship, and the Four Bar Moustache bridge and floral pickguard add a stage-ready aesthetic that demands attention. Experience the power of the Super Jumbo body, designed to cut through the loudest acoustic ensembles. The SJ-200 delivers a well-balanced, loud, and clear sound that will leave you breathless. Whether you're a rocker or a country picker, this guitar offers outstanding sound, projection, build quality, and jaw-dropping looks that will turn heads."
            }
          </p>
          <p>
            {
              "Descriptin Discover the extraordinary with the Gibson Custom Shop Murphy Lab 1957 SJ-200 'Light Aged Vintage Sunburst' Acoustic Guitar. This isn't just a guitar; it's a masterful work of art meticulously crafted to bring the vintage vibe and feel of a classic to your fingertips."
            }
          </p>
        </div>
      </div>
    );
  };

  return (
    <div className={`nc-ProductDetailPage `}>
      {/* MAIn */}
      <main className="container mt-5 lg:mt-11">
        <div className="lg:flex">
          {/* CONTENT */}
          <div className="w-full lg:w-[25%] ">
            {/* HEADING */}
            <div className="relative">
              <div className="aspect-w-8 aspect-h-16 relative">
                <Image
                  fill
                  sizes="(max-width: 640px) 100vw, 33vw"
                  src={LIST_IMAGES_DEMO[0]}
                  className="w-full rounded-2xl object-cover"
                  alt="product detail 1"
                />
              </div>
              {/* META FAVORITES */}
              <LikeButton className="absolute right-3 top-3 " />
            </div>
            <div className="grid grid-cols-2 gap-3 mt-3 sm:gap-6 sm:mt-6 xl:gap-8 xl:mt-8">
              {[LIST_IMAGES_DEMO[1], LIST_IMAGES_DEMO[2]].map((item, index) => {
                return (
                  <div
                    key={index}
                    className="aspect-w-12 xl:aspect-w-12 2xl:aspect-w-11 aspect-h-16 relative"
                  >
                    <Image
                      sizes="(max-width: 520px) 100vw, 30vw"
                      fill
                      src={item}
                      className="w-full rounded-2xl object-cover"
                      alt="product detail 1"
                    />
                  </div>
                );
              })}
            </div>
          </div>

          {/* SIDEBAR */}
          <div className="w-full lg:w-[45%] pt-10 lg:pt-0 lg:pl-7 xl:pl-9 2xl:pl-10">
            {renderSectionContent()}
          </div>
        </div>

        {/* DETAIL AND REVIEW */}
        <div className="mt-12 sm:mt-16 space-y-10 sm:space-y-16">
          {renderDetailSection()}
        </div>
      </main>
    </div>
  );
};

export default ProductDetailPage;
