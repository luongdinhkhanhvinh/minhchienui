import React from "react";
import SectionSliderProductCard from "@/components/SectionSliderProductCard";
import SectionGridFeatureItems from "@/components/SectionGridFeatureItems";
import SectionHero3 from "@/components/SectionHero/SectionHero3";
import { PRODUCTS_STRING_SIX, PRODUCTS_STRING_SIX_TWO } from "@/data/data";
import SectionHero from "@/components/SectionHero/SectionHero";

function PageHome() {
  return (
    <div className="nc-PageHome relative overflow-hidden">
      <SectionHero />
      <div className="container relative space-y-24 my-24 lg:my-20">
        <SectionGridFeatureItems
          heading="Acoustic Guitars"
          headingClassName="text-white my-16"
          data={PRODUCTS_STRING_SIX}
        />

        <SectionGridFeatureItems
          heading="Articles and Guides"
          headingClassName="text-white my-16"
          data={PRODUCTS_STRING_SIX}
        />
      </div>
    </div>
  );
}

export default PageHome;
