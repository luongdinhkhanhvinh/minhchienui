import React from "react";
import SectionHero2 from "@/components/SectionHero/SectionHero2";
import SectionSliderProductCard from "@/components/SectionSliderProductCard";
import SectionGridFeatureItems from "@/components/SectionGridFeatureItems";
import SectionHero3 from "@/components/SectionHero/SectionHero3";
import { PRODUCTS_STRING_SIX_TWO } from "@/data/data";

function PageHome() {
  return (
    <div className="nc-PageHome relative overflow-hidden">
      <SectionHero2 />
      <div className="container relative space-y-24 my-24 lg:space-x-20 lg:my-20">
        <SectionSliderProductCard
          heading="New Product"
          subHeading=""
          headingClassName={"text-white py-4"}
        />
      </div>

      <SectionHero3 />
      <div className="container relative space-y-24 my-24 lg:my-20">
        <SectionGridFeatureItems
          heading="6 String Acoustics Guitars"
          headingClassName="text-white my-16"
          data={PRODUCTS_STRING_SIX_TWO}
          showMore
        />

        <SectionGridFeatureItems
          heading="Banjos and Mandolins"
          headingClassName="text-white my-16"
          data={PRODUCTS_STRING_SIX_TWO}
          showMore
        />
      </div>
    </div>
  );
}

export default PageHome;
