import Logo from "@/shared/Logo/Logo";
import { CustomLink } from "@/data/types";
import React from "react";

export interface WidgetFooterMenu {
  id: string;
  title: string;
  menus: CustomLink[];
}

const widgetMenus: WidgetFooterMenu[] = [
  {
    id: "5",
    title: "",
    menus: [
      { href: "/", label: "About Us" },
      { href: "/", label: "Repairs & Servicing" },
      { href: "/", label: "About Us" },
      { href: "/", label: "Ordering, Shipping T's&C's / Privacy" },
      { href: "/", label: "Preowned and Vintage" },
      { href: "/", label: "Accessories" },
      { href: "/", label: "About Zip" },
    ],
  },
  {
    id: "1",
    title: "",
    menus: [
      { href: "/", label: "Shop Now" },
      { href: "/", label: "New Arrivals" },
      { href: "/", label: "Steel string Guitars" },
      { href: "/", label: "Classical Guitars" },
      { href: "/", label: "Electric Guitars" },
      { href: "/", label: "Preowned and Vintage" },
      { href: "/", label: "All Guitars" },
    ],
  },
  {
    id: "2",
    title: "",
    menus: [
      { href: "/", label: "Contact Us" },
      { href: "/", label: "03 9699 5691" },
      { href: "/", label: "info@acousticcentre.com.au" },
      { href: "/", label: "206 Park Street South" },
    ],
  },
  {
    id: "4",
    title: "",
    menus: [
      { href: "/", label: "Opening Hours" },
      { href: "/", label: "Monday to fiday:10am to 5ams" },
      { href: "/", label: "Saturday: 10am to 4pm" },
      { href: "/", label: "Sunday:Closed" },
    ],
  },
];

const Footer: React.FC = () => {
  const renderWidgetMenuItem = (menu: WidgetFooterMenu, index: number) => {
    return (
      <div key={index} className="text-sm">
        <h2 className="font-semibold text-neutral-700 dark:text-neutral-200">
          {menu.title}
        </h2>
        <ul className="mt-5 space-y-4">
          {menu.menus.map((item, index) => (
            <li key={index}>
              <a
                key={index}
                className="text-neutral-6000 dark:text-neutral-300 hover:text-white white:hover:text-white"
                href={item.href}
                target="_blank"
                rel="noopener noreferrer"
              >
                {item.label}
              </a>
            </li>
          ))}
        </ul>
      </div>
    );
  };

  return (
    <div className="nc-Footer relative py-20 lg:pt-28 lg:pb-24 border-t border-neutral-200 dark:border-neutral-700">
      <div className="container grid grid-cols-2 gap-y-10 gap-x-5 sm:gap-x-8 md:grid-cols-4 lg:grid-cols-5 lg:gap-x-10 ">
        <div className="grid grid-cols-4 gap-5 col-span-2 md:col-span-4 lg:md:col-span-1 lg:flex lg:flex-col">
          <div className="col-span-2 md:col-span-1">
            <Logo />
          </div>
        </div>
        {widgetMenus.map(renderWidgetMenuItem)}
      </div>
    </div>
  );
};

export default Footer;
