"use client";

import React, { FC } from "react";
import ProductCard from "@/components/ProductCard";
import ButtonPrimary from "@/shared/Button/ButtonPrimary";
import { Product, PRODUCTS_STRING_SIX } from "@/data/data";
import Heading from "./Heading/Heading";
import ncNanoId from "@/utils/ncNanoId";
import { useRouter } from "next/navigation";

//
export interface SectionGridFeatureItemsProps {
  data?: Product[];
  heading?: string;
  headingClassName?: string;
  showMore?: boolean;
}

const SectionGridFeatureItems: FC<SectionGridFeatureItemsProps> = ({
  data = PRODUCTS_STRING_SIX,
  headingClassName,
  heading,
  showMore = false,
}) => {
  const router = useRouter();

  return (
    <div className="nc-SectionGridFeatureItems relative" key={ncNanoId()}>
      <Heading className={headingClassName}>{heading || "Products"}</Heading>
      <div
        className={`grid gap-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 `}
      >
        {data.map((item, index) => (
          <ProductCard data={item} key={index} />
        ))}
      </div>
      {showMore && (
        <div className="flex mt-16 justify-center items-center">
          <ButtonPrimary onClick={() => router.push("/products")} >
            Show me more
          </ButtonPrimary>
        </div>
      )}
    </div>
  );
};

export default SectionGridFeatureItems;
