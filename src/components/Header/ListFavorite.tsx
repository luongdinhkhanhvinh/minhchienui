"use client";

import { Popover } from "@/app/headlessui";
import heartIcon from "@/images/heart-svgrepo-com.svg";
import Image from "next/image";

export default function ListFavorite() {
  return (
    <Popover className="relative">
      <Popover.Button
        className={`group w-10 h-10 sm:w-12 sm:h-12 hover:bg-slate-100 dark:hover:bg-slate-800 rounded-full inline-flex items-center justify-center focus:outline-none relative`}
      >
         <Image
          className={`block h-8 sm:h-10 w-auto`}
          src={heartIcon}
          alt="heart"
          sizes="48px"
          priority
        />
      </Popover.Button>
    </Popover>
  );
}
