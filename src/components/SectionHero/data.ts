import { StaticImageData } from "next/image";
import { Route } from "@/routers/types";

interface Hero2DataType {
  image: StaticImageData | string;
  heading: string;
  subHeading: string;
  btnText: string;
  btnLink: Route;
}

export const HERO2_DEMO_DATA: Hero2DataType[] = [
  {
    image: "",
    heading: "Welcome to the world of guitars",
    subHeading: "",
    btnText: "",
    btnLink: "/",
  },
];

export const HERO3_DEMO_DATA: Hero2DataType[] = [
  {
    image: "",
    heading: "Welcome to the world of guitars",
    subHeading: "",
    btnText: "",
    btnLink: "/",
  },
];
