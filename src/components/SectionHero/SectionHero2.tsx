"use client";

import React, { FC } from "react";
import backgroundLineSvg from "@/images/bg_hero.png";
import Image from "next/image";
import { HERO2_DEMO_DATA as DATA } from "./data";

export interface SectionHero2Props {
  className?: string;
}


const SectionHero2: FC<SectionHero2Props> = ({ className = "" }) => {
  const renderItem = (index: number) => {
    const item = DATA[index];
    return (
      <div
        className={`nc-SectionHero2Item nc-SectionHero2Item--animation flex flex-col-reverse lg:flex-col relative overflow-hidden ${className}`}
        key={index}
      >
        {/* BG */}
        <div className="absolute inset-0 bg-black">
          <Image
            fill
            sizes="(max-width: 768px) 100vw, 50vw"
            className="absolute w-full h-full object-contain"
            src={backgroundLineSvg}
            alt="hero"
          />
        </div>

        <div className="relative container pb-0 pt-14 sm:pt-20 lg:py-44">
          <div
            className={`relative z-[1] w-full max-w-3xl space-y-8 sm:space-y-14 nc-SectionHero2Item__left`}
          >
            <div className="space-y-5 sm:space-y-6">
              <h2 className="nc-SectionHero2Item__heading font-semibold text-3xl sm:text-4xl md:text-5xl xl:text-6xl 2xl:text-7xl !leading-[114%] text-white ">
                {item.heading}
              </h2>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return <>{DATA.map((_, index) => renderItem(index))}</>;
};

export default SectionHero2;
