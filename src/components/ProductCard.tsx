"use client";

import React, { FC } from "react";
import LikeButton from "./LikeButton";
import { Product, PRODUCTS_STRING_SIX } from "@/data/data";

import ButtonPrimary from "@/shared/Button/ButtonPrimary";
import Link from "next/link";
import NcImage from "@/shared/NcImage/NcImage";

export interface ProductCardProps {
  className?: string;
  data?: Product;
  isLiked?: boolean;
}

const ProductCard: FC<ProductCardProps> = ({
  className = "",
  data = PRODUCTS_STRING_SIX[1],
  isLiked,
}) => {
  const { name, image } = data;

  return (
    <>
      <div
        className={`nc-ProductCard relative flex flex-col bg-transparent ${className}`}
      >
        <Link href={"/product-detail"} className="absolute inset-0"></Link>

        <div className="relative flex-shrink-0 bg-transparent dark:bg-slate-300 rounded-3xl overflow-hidden z-1 group border-solid border border-gray-300">
          <Link href={"/product-detail"} className="block">
            <NcImage
              containerClassName="flex aspect-w-8 aspect-h-12 w-full h-0"
              src={image}
              className="object-cover w-full h-full drop-shadow-xl"
              fill
              sizes="(max-width: 640px) 100vw, (max-width: 1200px) 50vw, 40vw"
              alt="product"
            />
          </Link>
          <LikeButton liked={isLiked} className="absolute top-3 end-3 z-10" />
        </div>

        <div className="space-y-4 px-2.5 pt-5 pb-2.5">
          <div>
            <h2 className="nc-ProductCard__title text-white font-semibold transition-colors">
              {name}
            </h2>
            <p className={`text-sm text-slate-500 dark:text-slate-400 mt-1 `}>
              {"2.700.000 VND"}
            </p>
          </div>

          <div className="flex justify-between items-end ">
            <div className="flex items-center mb-0.5"></div>
            <ButtonPrimary>Buy now</ButtonPrimary>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProductCard;
