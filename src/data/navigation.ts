import { NavItemType } from "@/shared/Navigation/NavigationItem";
import ncNanoId from "@/utils/ncNanoId";

export const MEGAMENU_TEMPLATES: NavItemType[] = [
  {
    id: ncNanoId(),
    href: "/",
    name: "Home Page",
    children: [
      { id: ncNanoId(), href: "/", name: "Home  1" },
      { id: ncNanoId(), href: "/", name: "Header  1" },
      { id: ncNanoId(), href: "/", name: "Coming Soon" },
    ],
  },
];

const OTHER_PAGE_CHILD: NavItemType[] = [
  {
    id: ncNanoId(),
    href: "/",
    name: "Home Demo 1",
  },
  {
    id: ncNanoId(),
    href: "/product-detail",
    name: "Product Pages",
    type: "dropdown",
    children: [
      {
        id: ncNanoId(),
        href: "/product-detail",
        name: "Product detail 1",
      },
    ],
  },
  {
    id: ncNanoId(),
    href: "/search",
    name: "Search Page",
  },
];

export const NAVIGATION_DEMO_2: NavItemType[] = [
  {
    id: ncNanoId(),
    href: "/products",
    name: "Acoustic",
  },
  {
    id: ncNanoId(),
    href: "/products",
    name: "Electric",
  },
  {
    id: ncNanoId(),
    href: "/products",
    name: "Brands",
  },
  {
    id: ncNanoId(),
    href: "/products",
    name: "Guids",
  },
  {
    id: ncNanoId(),
    href: "/products",
    name: "Accessories",
  },
];
